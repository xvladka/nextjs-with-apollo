import Link from 'next/link';

import { useViewerQuery } from '../lib/viewer.graphql';
import withApollo from '../lib/with-apollo';

const Index = () => {
  const { data, loading } = useViewerQuery();

  if (loading) {
    return <div>Loading....</div>;
  }

  if (data) {
    const { viewer } = data;
    return (
      <div>
        You're signed in as {viewer.name} and you're {viewer.status}
        <hr />
        goto{' '}
        <Link href="/about">
          <a>static</a>
        </Link>{' '}
        page.
        <hr />
        Graphql playground: <a href="/api/graphql">/api/graphql</a>
        <hr />
        Based on example from{' '}
        <a href="https://github.com/zeit/next.js/tree/canary/examples/with-typescript-graphql">
          https://github.com/zeit/next.js/tree/canary/examples/with-typescript-graphql
        </a>
      </div>
    );
  }

  return <div>...</div>;
};

export default withApollo(Index);
